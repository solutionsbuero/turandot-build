# Move to file as working directory
cd "$(dirname "$0")"

# Get version number
TVERSION=$(cat turandot/setup.py | grep "version" | perl -pe '($_)=/([0-9]+([.][0-9]+)+)/')
TREVISION="${TVERSION}-1"
# echo $TREVISION

# Get architecture
ARCH=$(dpkg --print-architecture)
# echo $ARCH

# Assemble package name
TNAME="turandot_${TREVISION}_${ARCH}"
# echo $TNAME

# move wd to debian folder
if [ ! -d "./deb" ]
then
    mkdir deb
fi
cd deb

# remove old content
rm -rf *

# create folder for new version
mkdir $TNAME

# Copy prepared assets
cp -r ../deb_assets/* $TNAME

# move wd to package folder
cd $TNAME

# Write current revision number to control file
search=$(cat DEBIAN/control | grep "Version")
# echo $search
replace="Version: ${TREVISION}"
# echo $replace
sed -i "s/$search/$replace/" DEBIAN/control

# Create opt folder structure
if [ -d "./opt/turandot" ]
    then
        rm -rf opt/turandot/*
else
    mkdir -p opt/turandot
fi
if [ -d "./opt/citeproc-js-server" ]
    then
        rm -rf opt/citeproc-js-server/*
else
    mkdir -p opt/citeproc-js-server
fi

# Update Turandot files
cp -Rf ../../turandot/dist/Turandot/* opt/turandot/

# Copy citeproc-js-server files to package
echo "BUILDING CITEPROC..."
cp -Rf ../../citeproc-js-server/* opt/citeproc-js-server/
cat ../../assets/citeproc-package.json > opt/citeproc-js-server/package.json

# Build citeproc executable
cd opt/citeproc-js-server
npm install .
pkg --target node10-linux  --output ./citeproc-js-server .
chmod +x ./citeproc-js-server

# Cleaning up unnecessary citeproc assets
echo "working dir:"
pwd
rm -rf node_modules
rm -rf test
rm csl/*.csl
rm csl/dependent/*.csl

cd ../..

# Finally, build package
echo "CREATING DEB PACKAGE..."
cd ..
dpkg-deb --build $TNAME

echo "DONE!"

