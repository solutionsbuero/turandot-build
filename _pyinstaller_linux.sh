# Set file dir as working directory
cd "$(dirname "$0")"
pwd
# Move to application directory
cd turandot
pwd
# Check if venv exists; create if not
echo "PREPARING PYTHON ENVIRONMENT..."
if [ ! -d "./venv" ]
then
    python3 -m venv venv
fi

# Activate venv
source venv/bin/activate

# Install/update application & dependencies
pip3 install -r requirements.txt .

which pyinstaller
# Run PyInstaller
echo "RUNNING PYINSTALLER..."
pyinstaller --clean --noconfirm Turandot_linux.spec
