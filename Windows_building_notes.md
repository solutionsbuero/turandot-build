# Build Windows amd64 Binary

## Important

- _Do NOT build a venv, pygobject installation fails in venv!_

## Install & run msys2

- Download & install msys2 binary from [here](https://www.msys2.org/)

## Dependencies

- Install Microsoft Visual C++ Redistributable from [here](https://support.microsoft.com/en-us/topic/the-latest-supported-visual-c-downloads-2647da03-1eea-4433-9aff-95f26a218cc0)
- For some reason, also install Visual Studio 2013 (VC++ 12.0) from [here](https://support.microsoft.com/en-us/topic/update-for-visual-c-2013-redistributable-package-d8ccd6a5-4e26-c290-517b-8da6cfdf4f10)
- Run `C:\msys64\mingw64.exe`
- Install Python & pygobject: ct. [pygobject doc](https://pygobject.readthedocs.io/en/latest/getting_started.html#windows-logo-windows)

```bash
pacman -S mingw-w64-x86_64-gtk3 mingw-w64-x86_64-python3 mingw-w64-x86_64-python3-gobject
```

- Install other dependencies
- Install gcc (dependency for peewee, it seems)
- Install python-lxml (dependency for Beautiful Soup)

```bash
pacman -S git mingw-w64-x86_64-python-pip mingw-w64-x86_64-gcc mingw-w64-x86_64-python-lxml
```

#### Installation merged:

```bash
pacman -S mingw-w64-x86_64-gtk3 mingw-w64-x86_64-python3 mingw-w64-x86_64-python3-gobject git mingw-w64-x86_64-python-pip mingw-w64-x86_64-gcc mingw-w64-x86_64-python-lxml
```

## Clone 

_Do NOT build a venv, pygobject installation fails in venv!_

```bash
git clone https://gitlab.com/solutionsbuero/turandot.git
```

## Run pyinstaller

```bash
cd turandot
pyinstaller What-ever.spec
```
